import pdf_rules
import re

pdf = pdf_rules.PDF('tests/test_txt.txt')

# print(pdf)

## top level data
pdf.add_field(
        'acc',
        lambda rd, i, l: 'Account' in l,
        lambda rd, i, l: l[-7:])


## Level 1
pdf.add_level(
        lambda rd, i, l: 'Charges' in l,
        lambda rd, i, l: l == '.')
pdf.add_field(
        'Address',
        lambda rd, i, l: 'Charges' in l,
        lambda rd, i, l: l.split(' - ')[1],
        level=1)


## Level 2
pdf.add_level(
        lambda rd, i, l: re.search(r'[A-Z]{2}/\d{4}/[A-Z]', l),
        lambda rd, i, l: False)
pdf.add_field(
        'ID',
        lambda rd, i, l: re.search(r'[A-Z]{2}/\d{4}/[A-Z]', l),
        lambda rd, i, l: l.strip(),
        level=2)


## Level 3
pdf.add_level(
        lambda rd, i, l: re.search(r'\d{2}/\d{2}/\d{4}', l),
        lambda rd, i, l: True)
pdf.add_field(
        'Cost',
        lambda rd, i, l: True,
        lambda rd, i, l: pdf_rules.listify(l)[-1],
        level=3)


## show the PDF structure, press q to exit
# uncomment to try it out
pdf.show()


## Access specific values found by the PDF class

# we will add another field to find the invoice date
pdf.add_field(
        'invoice_date',
        lambda rd, i, l: 'Invoice Date' in l,
        lambda rd, i, l: pdf_rules.listify(l)[-1])

# you can access this by directly accessing
# pdf.hierarchy[level][row]['data']['field_name']
inv_date = pdf.hierarchy[0][0]['data']['invoice_date']


## Adding fallbacks
regex = r'\d{2}/\d{2}/\d{4}'

pdf.add_field(
        'start_date',
        lambda rd, i, l: re.search(regex, l),
        lambda rd, i, l: re.findall(regex, l)[0],
        level=3,
        fallback=pdf.last_entry('invoice_date'))
pdf.add_field(
        'end_date',
        lambda rd, i, l: re.search(regex, l),
        lambda rd, i, l: re.search(regex, l)[1],
        level=3,
        fallback=pdf.last_entry('invoice_date'))



## convert to CSV and write to file
csv = pdf_rules.CSV(pdf)
csv.write()
print(csv)

