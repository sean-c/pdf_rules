import sys
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

def dep():
    if 'win' in sys.platform:
        return ['listify']
    else:
        return ['listify', 'python-poppler', 'pdftotext']


setuptools.setup(
        name="pdf_rules",
        version="1.3.0",
        author="Sean Clouston",
        author_email="sean.clouston@tutanota.com",
        license="GPL",
        description="Turn PDFs into CSVs by defining rules",
        long_description=long_description,
        long_description_content_type="text/markdown",
        keywords=["automation", "business", "pdf", "processing"],
        url='https://gitlab.com/sean-c/pdf_rules',
        packages=setuptools.find_packages(),
        entry_points={'console_scripts': ['pdf-rules=pdf_rules.cmd:main']},
        classifiers=[
            "Development Status :: 4 - Beta",
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
            "Operating System :: OS Independent",
            "Topic :: Office/Business",
            "Topic :: Text Processing :: General",
            "Topic :: Utilities",
            "Intended Audience :: End Users/Desktop"
        ],
        python_requires='>=3.6',
        install_requires=dep(),
        test_suite='nose.collector',
        tests_require=['nose'],
)

