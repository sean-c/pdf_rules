.
Account             ABC1234
Invoice Number      1234567890
Invoice Date        01/01/2020
.
Charges:    -    1234 Fake Street, London W15 6GH
    IG/1234/H
        Installation        1        01/01/2020 - 31/01/2020        £100.00
        Credit              1        01/01/2020 - 01/01/2020        -$23.00
        Rental              1        01/01/2020 - 31/01/2020        £50.00
    ID/5678/I
        Rental              1        01/01/2020 - 31/01/2020        £52.00
    RD/9012/P
        Rental              1        01/01/2020 - 31/01/2020        £48.00
.
Charges:    -    5678 Fake Ave., Glasgow G3 6HJ
    DH/0471/U
        Rental              1        01/01/2020 - 31/01/2020        £64.00
    JF/8364/N
        Rental              1        01/01/2020 - 31/01/2020        £83.00
    HD/1684/Q
        Rental              1        01/01/2020 - 31/01/2020        £45.00
.
.
