import unittest
from unittest.mock import Mock, MagicMock, patch, mock_open
from reportlab.pdfgen.canvas import Canvas
import pdf_rules

import io
import sys
import types
import re

test_pdf = '''.
Account             ABC1234
Invoice Number      1234567890
Invoice Date        01/01/2020
.
Charges:    -    1234 Fake Street, London W15 6GH
    IG/1234/H
        Installation        1        01/01/2020 - 31/01/2020        £100.00
        Credit              1        01/01/2020 - 01/01/2020        -$23.00
        Rental              1        01/01/2020 - 31/01/2020        £50.00
    ID/5678/I
        Rental              1        01/01/2020 - 31/01/2020        £52.00
    RD/9012/P
        Rental              1        01/01/2020 - 31/01/2020        £48.00
    IN/5724/O
        Rental              1        01/01/2020                     -£324.00
.
Charges:    -    5678 Fake Ave., Glasgow G3 6HJ
    DH/0471/U
        Rental              1        01/01/2020 - 31/01/2020        £64.00
    JF/8364/N
        Rental              1        01/01/2020 - 31/01/2020        £83.00
    HD/1684/Q
        Rental              1        01/01/2020 - 31/01/2020        £45.00
.
.'''


# Use this to create test_pdf.pdf, used to test the PDF class

#canvas = Canvas('test_pdf2.pdf')
#h = 720
#for line in test_pdf.split('\n'):
#    print(line)
#    canvas.drawString(72, h, line)
#    h -= 10
#canvas.save()


test_pdf_bin = str.encode(test_pdf)


test_pdf_import_reader = ['.',
'Account           ABC1234',
'Invoice Number       1234567890',
'Invoice Date       01/01/2020',
'.',
'Charges: - 1234 Fake Street, London W15 6GH',
'   IG/1234/H',
'     Installation     1     01/01/2020 - 31/01/2020    £100.00',
'     Credit          1     01/01/2020 - 01/01/2020    -$23.00',
'     Rental           1    01/01/2020 - 31/01/2020     £50.00',
'   ID/5678/I',
'     Rental           1    01/01/2020 - 31/01/2020    £52.00',
'   RD/9012/P',
'     Rental           1    01/01/2020 - 31/01/2020    £48.00',
'   IN/5724/O',
'     Rental           1    01/01/2020                 -£324.00',
'.',
'Charges: - 5678 Fake Ave., Glasgow G3 6HJ',
'   DH/0471/U',
'     Rental           1    01/01/2020 - 31/01/2020    £64.00',
'   JF/8364/N',
'     Rental           1    01/01/2020 - 31/01/2020    £83.00',
'   HD/1684/Q',
'     Rental           1    01/01/2020 - 31/01/2020    £45.00',
'.',
'.',
'\x0c']


test_txt_import_reader = ['.',
'Account             ABC1234',
'Invoice Number      1234567890',
'Invoice Date        01/01/2020',
'.',
'Charges:    -    1234 Fake Street, London W15 6GH',
'    IG/1234/H',
'        Installation        1        01/01/2020 - 31/01/2020        £100.00',
'        Credit              1        01/01/2020 - 01/01/2020        -$23.00',
'        Rental              1        01/01/2020 - 31/01/2020        £50.00',
'    ID/5678/I',
'        Rental              1        01/01/2020 - 31/01/2020        £52.00',
'    RD/9012/P',
'        Rental              1        01/01/2020 - 31/01/2020        £48.00',
'    IN/5724/O',
'        Rental              1        01/01/2020                     -£324.00',
'.',
'Charges:    -    5678 Fake Ave., Glasgow G3 6HJ',
'    DH/0471/U',
'        Rental              1        01/01/2020 - 31/01/2020        £64.00',
'    JF/8364/N',
'        Rental              1        01/01/2020 - 31/01/2020        £83.00',
'    HD/1684/Q',
'        Rental              1        01/01/2020 - 31/01/2020        £45.00',
'.',
'.']


print_pdf = '''0 | .
1 | Account             ABC1234
2 | Invoice Number      1234567890
3 | Invoice Date        01/01/2020
4 | .
5 | Charges:    -    1234 Fake Street, London W15 6GH
6 |     IG/1234/H
7 |         Installation        1        01/01/2020 - 31/01/2020        £100.00
8 |         Credit              1        01/01/2020 - 01/01/2020        -$23.00
9 |         Rental              1        01/01/2020 - 31/01/2020        £50.00
10 |     ID/5678/I
11 |         Rental              1        01/01/2020 - 31/01/2020        £52.00
12 |     RD/9012/P
13 |         Rental              1        01/01/2020 - 31/01/2020        £48.00
14 |     IN/5724/O
15 |         Rental              1        01/01/2020                     -£324.00
16 | .
17 | Charges:    -    5678 Fake Ave., Glasgow G3 6HJ
18 |     DH/0471/U
19 |         Rental              1        01/01/2020 - 31/01/2020        £64.00
20 |     JF/8364/N
21 |         Rental              1        01/01/2020 - 31/01/2020        £83.00
22 |     HD/1684/Q
23 |         Rental              1        01/01/2020 - 31/01/2020        £45.00
24 | .
25 | .\n'''





class TestPDFImport(unittest.TestCase):
    '''Test importing documents into PDF'''

    @patch('pdf_rules.PDF.str2lst', return_value=test_txt_import_reader)
    @patch('mimetypes.guess_type', return_value=(None, None))
    def test_PDF_import_str(self, m_none, m_str2lst):
        pdf = pdf_rules.PDF(test_pdf)
        m_none.assert_called_once_with(test_pdf)
        self.assertEqual(pdf.path, './')
        self.assertEqual(pdf.reader, test_txt_import_reader)

    @patch('mimetypes.guess_type', return_value=(None, None))
    def test_PDF_import_lst(self, m_none):
        pdf = pdf_rules.PDF(test_txt_import_reader)
        m_none.assert_called_once_with(test_txt_import_reader)
        self.assertEqual(pdf.path, './')
        self.assertEqual(pdf.reader, test_txt_import_reader)

    @patch('pdf_rules.PDF.pdf2lst', return_value=test_txt_import_reader)
    @patch('mimetypes.guess_type', return_value=('document/pdf', 'pdf'))
    def test_PDF_import_pdf(self, m_pdf, m_pdf2lst):
        pdf = pdf_rules.PDF('test_pdf.pdf')
        m_pdf.assert_called_once_with('test_pdf.pdf')
        m_pdf2lst.assert_called_once()
        self.assertEqual(pdf.path, 'test_pdf.pdf')
        self.assertEqual(pdf.reader, test_txt_import_reader)

    @patch('pdf_rules.PDF.txt2lst', return_value=test_txt_import_reader)
    @patch('mimetypes.guess_type', return_value=('text/plain', 'text'))
    def test_PDF_import_txt(self, m_txt, m_txt2lst):
        pdf = pdf_rules.PDF('test_txt.txt')
        m_txt.assert_called_once_with('test_txt.txt')
        m_txt2lst.assert_called_once()
        self.assertEqual(pdf.path, 'test_txt.txt')
        self.assertEqual(pdf.reader, test_txt_import_reader)

    @patch('mimetypes.guess_type', return_value=(None, None))
    def test_PDF_import_dict(self, m_none):
        with self.assertRaises(TypeError):
            pdf_rules.PDF({})
            m_none.assert_called_once_with({})



class TestPDFMacicMethods(unittest.TestCase):
    def test_PDF__str__(self):
        pdf = pdf_rules.PDF(test_pdf)

        stdout_cap = io.StringIO()
        sys.stdout = stdout_cap
        print(pdf)
        sys.stdout = sys.__stdout__

        self.assertEqual(stdout_cap.getvalue(), print_pdf)

    def test_PDF__repr__(self):
        pdf = pdf_rules.PDF(test_pdf)
        self.assertEqual(repr(pdf), str(test_pdf.split('\n')))

    def test_PDF__len__(self):
        pdf = pdf_rules.PDF(test_pdf)
        self.assertEqual(len(pdf), 26)

    def test_PDF__iter__(self):
        pdf = pdf_rules.PDF(test_pdf)
        self.assertEqual(iter(pdf), pdf)

    def test_PDF__next__(self):
        pdf = pdf_rules.PDF(test_pdf)
        i = iter(pdf)
        self.assertEqual(next(i), '.')
        self.assertEqual(next(i), 'Account             ABC1234')
        self.assertEqual(next(i), 'Invoice Number      1234567890')

    def test_PDF__getitem__(self):
        pdf = pdf_rules.PDF(test_pdf)
        self.assertEqual(pdf[0], '.')
        self.assertEqual(pdf[1], 'Account             ABC1234')
        self.assertEqual(pdf[2], 'Invoice Number      1234567890')

    def test_PDF__add__(self):
        extra = '''extra_row
another extra row'''

        self.maxDiff = None
        pdf = pdf_rules.PDF(test_pdf)
        pdf2 = pdf_rules.PDF(extra)
        pdf3 = pdf + pdf2
        pdf4 = pdf + extra
        pdf5 = pdf + ['extra_row', 'another extra row']

        new_reader = test_pdf + '\n' + extra
        pdfA = pdf_rules.PDF(new_reader)

        self.assertEqual(repr(pdf3), repr(pdfA))
        self.assertEqual(repr(pdf4), repr(pdfA))
        self.assertEqual(repr(pdf5), repr(pdfA))


class TestPDFConv(unittest.TestCase):
    try:
        import pdftotext
    except:
        print('could not import pdftotext, skipping test TestPDFConv')

    @patch.object(pdftotext, 'PDF')
    def test_PDF_pdf2lst(self, m_p2t):
        m_p2t.return_value = test_pdf.split('\n')
        with patch('builtins.open',
                    mock_open(read_data=test_pdf_bin)
                ) as m_open:
            pdf = pdf_rules.PDF('')
            pdf.path = 'test_pdf.pdf'
            reader = pdf.pdf2lst()

        m_open.assert_called_once_with('test_pdf.pdf', 'rb')
        m_p2t.assert_called_once_with(m_open.return_value)
        self.assertEqual(reader, test_txt_import_reader)



class TestPDFHelpers(unittest.TestCase):
    def test_PDF_txt2lst(self):
        with patch('builtins.open',
                    mock_open(read_data=test_pdf)
                ) as m_open:
            pdf = pdf_rules.PDF('')
            pdf.path = 'test_txt.txt'
            reader = pdf.txt2lst()

        m_open.assert_called_once_with('test_txt.txt','r')
        self.assertEqual(reader, test_txt_import_reader)

    def test_PDF_str2lst(self):
        pdf = pdf_rules.PDF('')
        pdf.path = 'this is a string'
        reader  = pdf.str2lst()
        self.assertEqual(reader, ['this is a string'])

    def test_PDF_write_reader(self):
        with patch('builtins.open', mock_open()) as m_open:
            pdf = pdf_rules.PDF('this is a string')
            pdf.write_reader()

        handle = m_open()
        handle.write.assert_called_once_with('0 | this is a string')

    def test_PDF_last_entry(self):
        '''PDF.last_entry('field') should return 
        'field' entry for last row'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.hierarchy = {
                0: {0: {'rows': [0, 23], 'data': {
                        'account': 'ABC1234'}}},
                    1: {0: {'rows': [5, 14], 'data': {
                            'address': '1234 Fake Street, London W15 6GH'}},
                        1: {'rows': [15, 22],'data': {
                            'address': '5678 Fake Ave., Glasgow G3 6HJ'}}}}

        self.assertEqual(pdf.last_entry('address'),
                '5678 Fake Ave., Glasgow G3 6HJ')

        pdf.hierarchy = {
                0: {0: {'rows': [0, 23], 'data': {
                        'account': 'ABC1234'}}},
                    1: {0: {'rows': [5, 14], 'data': {
                            'address': '1234 Fake Street, London W15 6GH'}},
                        1: {'rows': [15, 22],'data': {}}}}

        self.assertEqual(pdf.last_entry('address'),
                '1234 Fake Street, London W15 6GH')


class TestPDFLevels(unittest.TestCase):
    @patch.object(pdf_rules.PDF, 'process')
    def test_PDF_add_level(self, m_process):
        '''test adding one level'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.add_level('test_trigger_start',
                'test_trigger_end')

        m_process.assert_has_calls(['', ''])
        self.assertEqual(pdf.levels[1], ('test_trigger_start',
            'test_trigger_end'))

    @patch.object(pdf_rules.PDF, 'process')
    def test_PDF_add_level_twice(self, m_process):
        '''test adding two levels'''
        pdf = pdf_rules.PDF(test_pdf)

        pdf.add_level('test_trigger_start_1', 'test_trigger_end_1')
        pdf.add_level('test_trigger_start_2', 'test_trigger_end_2')

        m_process.assert_has_calls(['', '', ''])
        self.assertEqual(pdf.levels[1],
                ('test_trigger_start_1', 'test_trigger_end_1'))
        self.assertEqual(pdf.levels[2],
                ('test_trigger_start_2', 'test_trigger_end_2'))


class TestPDFFields(unittest.TestCase):
    @patch.object(pdf_rules.PDF, 'process')
    def test_PDF_add_field(self, m_process):
        '''test adding one filed to l0'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.add_field('test', 'test_trigger', 'test_rule')

        self.assertEqual(pdf.fields, {
            0: {
                'test': ('test_trigger', 'test_rule', None)}})
        m_process.assert_has_calls(['', ''])

    @patch.object(pdf_rules.PDF, 'process')
    def test_PDF_add_field_twice(self, m_process):
        '''test adding two fields to l0'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.add_field('test_1', 'test_trigger_1', 'test_rule_1')
        pdf.add_field('test_2', 'test_trigger_2', 'test_rule_2')

        self.assertEqual(pdf.fields, {
            0: {
                'test_1': ('test_trigger_1', 'test_rule_1', None),
                'test_2': ('test_trigger_2', 'test_rule_2', None)}})
        m_process.assert_has_calls(['', '', ''])

    @patch.object(pdf_rules.PDF, 'process')
    def test_PDF_add_field_twice_two_levels(self, m_process):
        '''test adding fields to l0 and l1'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.add_field('test_1', 'test_trigger_1', 'test_rule_1')
        pdf.add_field('test_2', 'test_trigger_2', 'test_rule_2', level=1)

        self.assertEqual(pdf.fields, {
            0: {'test_1': ('test_trigger_1', 'test_rule_1', None)},
            1: {'test_2': ('test_trigger_2', 'test_rule_2', None)}})
        m_process.assert_has_calls(['', '', ''])

    @patch.object(pdf_rules.PDF, 'process')
    def test_PDF_add_field_fallback(self, m_process):
        '''test the fallback is entered alongside the trigger/rule'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.add_field('test_1', False, 'test_rule_1',
                fallback='test_fallback_1')
        pdf.add_field('test_2', False, 'test_rule_2',
                fallback='test_fallback_2', level=1)

        self.assertEqual(pdf.fields, {
            0: {'test_1': (False, 'test_rule_1', 'test_fallback_1')},
            1: {'test_2': (False, 'test_rule_2', 'test_fallback_2')}})
        m_process.assert_has_calls(['', '', ''])



class TestPDFProcess(unittest.TestCase):
    def test_PDF_process_default(self):
        '''Level 0 and no fields'''
        pdf = pdf_rules.PDF(test_pdf)

        self.assertEqual(pdf.hierarchy,
                {0: {0: {'rows': [0, 25], 'data': {}}}})

    def test_PDF_process_add_one_level(self):
        '''Level 0, Level 1 and no fields'''
        l0_trigger_start = Mock(side_effect=lambda rd, i, l: i == 0)
        l0_trigger_end = Mock(side_effect=lambda rd, i, l: i == len(rd) - 1)
        l1_trigger_start = Mock(side_effect=lambda rd, i, l: 'Charges' in l)
        l1_trigger_end = Mock(side_effect=lambda rd, i, l: l == '.')

        pdf = pdf_rules.PDF(test_pdf)
        pdf.levels = {
                0: (l0_trigger_start, l0_trigger_end),
                1: (l1_trigger_start, l1_trigger_end)}

        pdf.process()

        self.assertEqual(pdf.hierarchy, {
                    0: {0: {'rows': [0, 25], 'data': {}}},
                    1: {0: {'rows': [5, 16], 'data': {}},
                        1: {'rows': [17, 24],'data': {}}}})
        self.assertLessEqual(
                len(l0_trigger_start.call_args_list), 46)

    def test_PDF_process_no_fail_trigger_use_next_line(self):
        '''Test that rd[i+1] can be used in trigger function'''
        pdf = pdf_rules.PDF(test_pdf)
        L1_trigger_start = Mock(side_effect=lambda rd, i, l: 'Charges' in rd[i+1])
        L1_trigger_end = Mock(side_effect=lambda rd, i, l: l == '.')

        pdf.add_level(L1_trigger_start, L1_trigger_end)

        self.assertNotEqual(pdf.hierarchy[1], {})

    def test_PDF_process_add_field_even_if_trigger_fail(self):
        '''ensure that the field still present in hierarchy even if 
        trigger doesn't trigger'''
        pdf = pdf_rules.PDF(test_pdf)
        m_trigger = Mock(side_effect=lambda rd, i, l: False)
        m_rule = Mock(side_effect=lambda rd, i, l: 'should not return')
        pdf.fields = {0: {'account': (m_trigger, m_rule, None)}}

        pdf.process()

        self.assertEqual(pdf.hierarchy[0], {
                0: {
                    'rows': [0, 25],
                    'data': {'account': None}}})
        m_rule.assert_not_called()

    def test_PDF_process_add_field_trigger_fallback(self):
        '''test the fallback is used when trigger doesn't trigger'''
        pdf = pdf_rules.PDF(test_pdf)
        m_trigger = Mock(side_effect=lambda rd, i, l: False)
        m_rule = Mock(side_effect=lambda rd, i, l: 'should not return')
        fallback = 'FALLBACK'

        pdf.fields = {0: {'account': (m_trigger, m_rule, 'FALLBACK')}}

        pdf.process()

        self.assertEqual(pdf.hierarchy[0], {
                0: {
                    'rows': [0, 25],
                    'data': {'account': 'FALLBACK'}}})
        m_rule.assert_not_called()

    def test_PDF_process_add_field_rule_fallback(self):
        '''test that fallback used when rule returns None'''
        pdf = pdf_rules.PDF(test_pdf)

        inv_date = '01/01/2020'

        pdf.add_level(
                lambda rd, i, l: re.search(r'\d{2}/\d{2}/\d{4}', l),
                lambda rd, i, l: True)
        
        def end_date(rd, i, l):
            d = re.findall(r'\d{2}/\d{2}/\d{4}', l)
            if len(d) < 2:
                return None
            else:
                return d[-1]

        pdf.add_field(
                'start_date',
                lambda rd, i, l: True,
                lambda rd, i, l: re.findall(r'\d{2}/\d{2}/\d{4}', l)[0],
                level=1)

        pdf.add_field(
                'end_date',
                lambda rd, i, l: True,
                lambda rd, i, l: end_date(rd, i, l),
                level=1,
                fallback=inv_date)

        self.assertEqual(pdf.hierarchy[1][6]['data']['start_date'], 
                            pdf.hierarchy[1][6]['data']['end_date'])

    def test_PDF_process_add_field_rule_fallback_on_exception(self):
        '''test fallback used on rule throwing exception'''
        pdf = pdf_rules.PDF(test_pdf)

        inv_date = '01/01/2020'

        pdf.add_level(
                lambda rd, i, l: re.search(r'\d{2}/\d{2}/\d{4}', l),
                lambda rd, i, l: True)
        

        pdf.add_field(
                'start_date',
                lambda rd, i, l: True,
                lambda rd, i, l: re.findall(r'\d{2}/\d{2}/\d{4}', l)[0],
                level=1)

        pdf.add_field(
                'end_date',
                lambda rd, i, l: True,
                lambda rd, i, l: aaaaaahhh,
                level=1,
                fallback=inv_date)

        self.assertEqual(pdf.hierarchy[1][6]['data']['start_date'], 
                            pdf.hierarchy[1][6]['data']['end_date'])

    def test_PDF_process_no_overlapping_tables(self):
        '''two tables on same level can't cover the same row'''
        pdf = pdf_rules.PDF(test_pdf)
        pdf.add_level(lambda rd, i, l: 'Charges' in l,
                     lambda rd, i, l: l == '.')

        r1 = set(pdf.hierarchy[1][0]['rows'])
        r2 = set(pdf.hierarchy[1][1]['rows'])
        i = r1.intersection(r2)
        self.assertTrue(i == set([]))

    def test_PDF_process_False_end_trigger(self):
        '''test that when False is passed as end_trigger in add_level,
        the tables end one line above the start of the next'''
        pdf = pdf_rules.PDF(test_pdf)

        pdf.add_level(
                lambda rd, i, l: 'Charges' in l,
                lambda rd, i, l: False)

        self.assertEqual(pdf.hierarchy[1], 
                            {
                                0: {'rows': [5, 16], 'data': {}},
                                1: {'rows': [17, 25], 'data': {}}
                            }
        )

    def test_PDF_process_True_end_trigger(self):
        '''test that when True is passed as end_trigger in add_level,
        the tables are one-liners'''
        pdf = pdf_rules.PDF(test_pdf)

        pdf.add_level(
                lambda rd, i, l: 'Charges' in l,
                lambda rd, i, l: True)

        self.assertEqual(pdf.hierarchy[1], 
                            {
                                0: {'rows': [5, 5], 'data': {}},
                                1: {'rows': [17, 17], 'data': {}}
                            }
        )








class TestCSVMagicMethods(unittest.TestCase):
    def test_CSV__str__(self):
        pdf = pdf_rules.PDF(test_pdf)
        pdf.hierarchy = {
            0: {0: {'rows': [0, 23], 'data': {
                    'account': 'ABC1234'}}}}
        csv = pdf_rules.CSV(pdf)

        stdout_cap = io.StringIO()
        sys.stdout = stdout_cap
        print(csv)
        sys.stdout = sys.__stdout__

        self.assertEqual(stdout_cap.getvalue(),'''
['account']
['ABC1234']
''')

    def test_CSV__len__(self):
        pass

    def test_CSV__repr__(self):
        pass

    def test_CSV__add__(self):
        extra = [['3', '4']]

        pdf = pdf_rules.PDF(test_pdf)
        csv = pdf_rules.CSV(pdf)
        csv.csv = [['hed1', 'hed2'],
                ['1', '2']]

        self.assertEqual(csv + extra, [['hed1', 'hed2'],
            ['1', '2'],
            ['3', '4']])

    def test_CSV__sub__(self):
        extra = ['3', '4']

        pdf = pdf_rules.PDF(test_pdf)
        csv = pdf_rules.CSV(pdf)
        csv.csv = [['hed1', 'hed2'],
                ['1', '2'],
                ['3', '4']]

        self.assertEqual(csv - extra, [['hed1', 'hed2'],
            ['1', '2']])

    def test_CSV__eq__(self):
        pdf = pdf_rules.PDF(test_pdf)
        pdf2 = pdf_rules.PDF(test_pdf)
        pdf3 = pdf_rules.PDF(test_pdf)
        csv = pdf_rules.CSV(pdf)
        csv2 = pdf_rules.CSV(pdf2)
        csv3 = pdf_rules.CSV(pdf3)

        csv.csv = [['h1', 'h2'],
                ['r1', 'r1']]
        csv2.csv = [['h1', 'h2'],
                ['r1', 'r1']]
        csv3.csv = [['h1', 'h2'],
                ['c1', 'c1']]

        self.assertTrue((csv == csv2))
        self.assertFalse((csv == csv3))

    def test_CSV__neq__(self):
        pass

class TestCSV(unittest.TestCase):
    @patch.object(pdf_rules, 'PDF')
    def test_CSV_base_lvl_no_data(self, m_PDF):
        m_PDF.hierarchy = {0: {0: {'rows': [0, 23], 'data': {}}}}

        csv = pdf_rules.CSV(m_PDF)

        self.assertEqual(csv.csv, [[],[]])

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_one_lvl_no_data(self, m_PDF):
        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {}}},
                    1: {0: {'rows': [5, 14], 'data': {}},
                        1: {'rows': [15, 22],'data': {}}}}
        csv = pdf_rules.CSV(m_PDF)

        self.assertEqual(csv.csv, [[], [], []])

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_two_lvl_no_data(self,m_PDF):
        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {}}},
                    1: {0: {'rows': [5, 14], 'data': {}},
                        1: {'rows': [15, 22],'data': {}}},
                    2: {0: {'rows': [6, 9],'data': {}},
                        1: {'rows': [10, 11],'data': {}},
                        2: {'rows': [12, 13],'data': {}},
                        3: {'rows': [16, 17],'data': {}},
                        4: {'rows': [18, 19],'data': {}},
                        5: {'rows': [20, 21],'data': {}}}}
        csv = pdf_rules.CSV(m_PDF)

        self.assertEqual(csv.csv, [[], [], [], [], [], [], []])

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_base_lvl_plus_data(self, m_PDF):
        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {
                        'account': 'ABC1234'}}}}
        csv = pdf_rules.CSV(m_PDF)

        self.assertEqual(csv.csv, [['account'], ['ABC1234']])

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_one_lvl_plus_data(self, m_PDF):
        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {
                        'account': 'ABC1234'}}},
                    1: {0: {'rows': [5, 14], 'data': {
                            'address': '1234 Fake Street, London W15 6GH'}},
                        1: {'rows': [15, 22],'data': {
                            'address': '5678 Fake Ave., Glasgow G3 6HJ'}}}}
        csv = pdf_rules.CSV(m_PDF)

        self.assertEqual(csv.csv, [['account', 'address'],
            ['ABC1234', '1234 Fake Street, London W15 6GH'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ']])

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_two_lvl_plus_data(self, m_PDF):
        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {
                            'account': 'ABC1234'}}},
                    1: {0: {'rows': [5, 14], 'data': {
                            'address': '1234 Fake Street, London W15 6GH'}},
                        1: {'rows': [15, 22],'data': {
                            'address': '5678 Fake Ave., Glasgow G3 6HJ'}}},
                    2: {0: {'rows': [6, 9],'data': {
                            'circuit': 'IG/1234/H'}},
                        1: {'rows': [10, 11],'data': {
                            'circuit': 'ID/5678/I'}},
                        2: {'rows': [12, 13],'data': {
                            'circuit': 'RD/9012/P'}},
                        3: {'rows': [16, 17],'data': {
                            'circuit': 'DH/0471/U'}},
                        4: {'rows': [18, 19],'data': {
                            'circuit': 'JF/8364/N'}},
                        5: {'rows': [20, 21],'data': {
                            'circuit': 'HD/1684/Q'}}}}

        csv = pdf_rules.CSV(m_PDF)

        self.assertEqual(csv.csv, [
            ['account', 'address', 'circuit'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'ID/5678/I'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'RD/9012/P'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'DH/0471/U'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'JF/8364/N'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'HD/1684/Q']
            ])

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_three_lvl_plus_data(self, m_PDF):
        d = [
            ['account', 'address', 'circuit', 'cost'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H', '£100.00'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H', '-£23.00'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H', '£50.00'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'ID/5678/I', '£52.00'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'RD/9012/P', '£48.00'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'DH/0471/U', '£64.00'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'JF/8364/N', '£83.00'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'HD/1684/Q', '£45.00']
            ]

        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {
                            'account': 'ABC1234'}}},
                    1: {0: {'rows': [5, 14], 'data': {
                            'address': '1234 Fake Street, London W15 6GH'}},
                        1: {'rows': [15, 22],'data': {
                            'address': '5678 Fake Ave., Glasgow G3 6HJ'}}},
                    2: {0: {'rows': [6, 9],'data': {
                            'circuit': 'IG/1234/H'}},
                        1: {'rows': [10, 11],'data': {
                            'circuit': 'ID/5678/I',}},
                        2: {'rows': [12, 13],'data': {
                            'circuit': 'RD/9012/P',}},
                        3: {'rows': [16, 17],'data': {
                            'circuit': 'DH/0471/U'}},
                        4: {'rows': [18, 19],'data': {
                            'circuit': 'JF/8364/N'}},
                        5: {'rows': [20, 21],'data': {
                            'circuit': 'HD/1684/Q'}}},
                    3: {0: {'rows': [7, 7],'data': {
                            'cost': '£100.00'}},
                        1: {'rows': [8, 8],'data': {
                            'cost': '-£23.00'}},
                        2: {'rows': [9, 9],'data': {
                            'cost': '£50.00'}},
                        3: {'rows': [11, 11],'data': {
                            'cost': '£52.00'}},
                        4: {'rows': [13, 13],'data': {
                            'cost': '£48.00'}},
                        5: {'rows': [17, 17],'data': {
                            'cost': '£64.00'}},
                        6: {'rows': [19, 19],'data': {
                            'cost': '£83.00'}},
                        7: {'rows': [21, 21],'data': {
                            'cost': '£45.00'}}}}

        csv = pdf_rules.CSV(m_PDF)
        self.assertEqual(csv.csv, d)

    @patch.object(pdf_rules, 'PDF')
    def test_CSV_three_lvl_plus_multi_data(self, m_PDF):
        d = [
            ['account', 'address', 'circuit', 'cost', 'description'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H', '£100.00', 'Installation'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H', '-£23.00', 'Credit'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'IG/1234/H', '£50.00', 'Rental'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'ID/5678/I', '£52.00', 'Rental'],
            ['ABC1234', '1234 Fake Street, London W15 6GH', 'RD/9012/P', '£48.00', 'Rental'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'DH/0471/U', '£64.00', 'Rental'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'JF/8364/N', '£83.00', 'Rental'],
            ['ABC1234', '5678 Fake Ave., Glasgow G3 6HJ', 'HD/1684/Q', '£45.00', 'Rental']
            ]

        m_PDF.hierarchy = {
                    0: {0: {'rows': [0, 23], 'data': {
                            'account': 'ABC1234'}}},
                    1: {0: {'rows': [5, 14], 'data': {
                            'address': '1234 Fake Street, London W15 6GH'}},
                        1: {'rows': [15, 22],'data': {
                            'address': '5678 Fake Ave., Glasgow G3 6HJ'}}},
                    2: {0: {'rows': [6, 9],'data': {
                            'circuit': 'IG/1234/H'}},
                        1: {'rows': [10, 11],'data': {
                            'circuit': 'ID/5678/I',}},
                        2: {'rows': [12, 13],'data': {
                            'circuit': 'RD/9012/P',}},
                        3: {'rows': [16, 17],'data': {
                            'circuit': 'DH/0471/U'}},
                        4: {'rows': [18, 19],'data': {
                            'circuit': 'JF/8364/N'}},
                        5: {'rows': [20, 21],'data': {
                            'circuit': 'HD/1684/Q'}}},
                    3: {0: {'rows': [7, 7],'data': {
                            'cost': '£100.00',
                            'description': 'Installation'}},
                        1: {'rows': [8, 8],'data': {
                            'cost': '-£23.00',
                            'description': 'Credit'}},
                        2: {'rows': [9, 9],'data': {
                            'cost': '£50.00',
                            'description': 'Rental'}},
                        3: {'rows': [11, 11],'data': {
                            'cost': '£52.00',
                            'description': 'Rental'}},
                        4: {'rows': [13, 13],'data': {
                            'cost': '£48.00',
                            'description': 'Rental'}},
                        5: {'rows': [17, 17],'data': {
                            'cost': '£64.00',
                            'description': 'Rental'}},
                        6: {'rows': [19, 19],'data': {
                            'cost': '£83.00',
                            'description': 'Rental'}},
                        7: {'rows': [21, 21],'data': {
                            'cost': '£45.00',
                            'description': 'Rental'}}}}

        csv = pdf_rules.CSV(m_PDF)
        self.assertEqual(csv.csv, d)




if __name__ == '__main__':
    unittest.main()

